import Vue from "vue";
import Vuex from "vuex";
import Axios from "axios";

Vue.use(Vuex);

interface ISource {
  id: string;
  name: string;
  category: string;
  country: string;
  description: string;
  language: string;
  url: string;
}

export const store = new Vuex.Store({
  state: {
    articles: {
      data: [],
      isLoading: true,
      sources: []
    },
    settings: {
      lang: "en"
    },
    filters: {
      sources: {
        string: "",
        data: <ISource[]>[]
      }
    }
  },
  mutations: {
    setArticles(state, payload) {
      state.articles.isLoading = false;
      state.articles.data = payload;
    },
    setSources(state, payload) {
      state.articles.sources = payload;
    },
    removeSourceFromFilter(state, payload: ISource) {
      state.filters.sources.data.forEach((item, key) => {
        if (item.name === payload.name) {
          state.filters.sources.data.splice(key, 1);
        }
      });
    },
    filterBySource(state, payload?: ISource) {
      if (payload) {
        state.filters.sources.data.push(payload);
      }

      const data = state.filters.sources.data;
      if (data.length == 0) {
        state.filters.sources.string = "";
        return;
      }
      state.filters.sources.string = "";
      data.forEach((item, key) => {
        if (state.filters.sources.string.length >= 1) {
          state.filters.sources.string += "," + item.id;
        } else {
          state.filters.sources.string += item.id;
        }
      });
    }
  },
  actions: {
    loadArticles(context) {
      context.state.articles.isLoading = true;
      let articles: Array<Object> = [];
      if (context.state.filters.sources.string.length > 0) {
        Axios.get(
          "https://newsapi.org/v2/top-headlines?sources=" +
            context.state.filters.sources.string +
            "&pageSize=100&apiKey=86b419223a7b4bb58ab79de33f2bcc9d"
        ).then(res => {
          articles = res.data.articles;
          setTimeout(() => {
            context.commit("setArticles", articles);
          }, 3000);
        });
        return true;
      }
      Axios.get(
        "https://newsapi.org/v2/top-headlines?language=" +
          context.state.settings.lang +
          "&pageSize=100&apiKey=86b419223a7b4bb58ab79de33f2bcc9d"
      ).then(res => {
        articles = res.data.articles;
        setTimeout(() => {
          context.commit("setArticles", articles);
        }, 3000);
      });
    },
    loadSources(context) {
      let sources: Array<Object> = [];
      Axios.get(
        "https://newsapi.org/v2/sources?language=" +
          context.state.settings.lang +
          "&apiKey=86b419223a7b4bb58ab79de33f2bcc9d"
      ).then(res => {
        sources = res.data.sources;
        setTimeout(() => {
          context.commit("setSources", sources);
        }, 3000);
      });
    }
  },
  modules: {}
});
